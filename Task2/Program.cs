﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Matrix matrix = new Matrix();

                int row = 0;
                int column= 0;

                Console.WriteLine("Enter row of matrix");
                string inputRow = Console.ReadLine();

                Console.WriteLine("Enter column of matrix");
                string inputColumn = Console.ReadLine();

                if (IsInt(inputColumn) && IsInt(inputRow))
                {
                    column = Int32.Parse(inputColumn);
                    row = Int32.Parse(inputRow);
                }
                else
                {
                    continue;
                }

                try
                {
                    matrix.Create(row, column);
                }
                catch(ArgumentOutOfRangeException exe)
                {
                    Console.WriteLine(exe.Message);
                }
                catch (Exception exe)
                {
                    Log.Write(exe);
                }

                matrix.Print();
                Console.WriteLine($"Sum of matrix diagonal: {matrix.SumMatrixDiagonal()}");
            }

        }

        public static bool IsInt(string text)
        {

            if (text != null)
            {
                int res;
                if (Int32.TryParse(text, out res) && res > 0)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("You must enter only number, above zero ");
                }

            }
            
            return false;
        }

    }
}
