﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Task2Tests")]
namespace Task2
{
    
    public class Matrix
    {
        internal int columnCount;

        internal int rowCount;

        internal int[,] matrix;

        public int[,] MatrixValue
        {
            get
            {
                return matrix;
            }
        }
         
        public void Create(int row, int column)
        {
            if (row <= 0 || column <= 0)
            {
                throw new ArgumentOutOfRangeException("All numbers must be above zero");
            }

            columnCount = column;
            rowCount = row;

            matrix = new int[rowCount, columnCount];

            Random rnd = new Random();

            for(int i = 0; i < rowCount; i++)
            {
                for(int j = 0; j < columnCount; j++)
                {
                    matrix[i, j] = rnd.Next(0, 101);
                }
            }
        }

        public void Print()
        {
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    if (i == j)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(matrix[i, j] + "\t");
                    }
                    else
                    {
                        Console.ResetColor();
                        Console.Write(matrix[i, j] + "\t");
                    }
                }
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        public int SumMatrixDiagonal()
        {
            int sum = 0;

            for (int i = 0; i < rowCount && i < columnCount; i++)
                    sum += matrix[i, i];
          
            return sum;
        }

    }
}
