﻿using NUnit.Framework;
using Task2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Task2.Tests
{
    public class MatrixTests: Matrix
    {
        

        [Test()]
        public void CreateMatrixTest()
        {
            //Arrange
            int row = 20;
            int column = 20;
            bool rangeRandom = true;

            //Act
            Matrix matrix = new Matrix();
            matrix.Create(row, column);

            for(int i = 0; i < row; i++) 
            { 
                for(int j = 0; j < column; j++)
                {
                    if(matrix.MatrixValue[i,j] > 100)
                    {
                        rangeRandom = false;
                    }
                }
            }

            //Assert
            Assert.IsTrue(rangeRandom);
        }


        [Test()]
        [TestCase(2, 2, 1)]
        [TestCase(2, 3, 1)]
        [TestCase(3, 3, 3)]
        [TestCase(4, 4, 6)]
        [TestCase(5, 3, 3)]
        [TestCase(6, 6, 15)]
        public void SumMatrixTest(int row, int column, int expectedSum)
        {

            //Act
            Matrix testMatrix = new Matrix();

            testMatrix.rowCount = row;
            testMatrix.columnCount = column;

            testMatrix.matrix = CreateTestMatrix(row, column);

            int sum = testMatrix.SumMatrixDiagonal();

            //Assert
            Assert.AreEqual(expectedSum, sum);
        }

        public int[,] CreateTestMatrix(int row, int column)
        {
            int[,] matrix = new int[row, column];

            for(int i = 0; i<row; i++)
            {
                for(int j = 0; j< column; j++)
                {
                    matrix[i, j] = j;
                }
            }
            return matrix;
        }
    }
}